<?php

/**
 * @file
 * Install Gallery Assist ImageCache.
 */

/**
 * Implementation of hook_requirements().
 */
 function gallery_assist_ic_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break at install time.
  $t = get_t();

  if ($phase == 'runtime') {
    $imagecache_dir = file_directory_path() .'/imagecache';

    if (!module_exists('imagecache') || !is_dir($imagecache_dir)) {
      $requirements['gallery_assist_imagecache'] = array(
        'title' => $t('Gallery Assist ImageCache'),
        'value' => $t('The module ImageCache must be INSTALLED before Gallery Assist ImageCache can create presets from existing assignments.'),
        'severity' => REQUIREMENT_ERROR,
      );
    }
  }
  return $requirements;
}

/**
 * Implementation of hook_install().
 */
function gallery_assist_ic_install() {
  variable_set('gallery_assist_use_imagecache', 1);

  $types = node_get_types('types');
  if (!array_key_exists('gallery_assist', $types)) {
    $types['gallery_assist']->type = 'gallery_assist';
  }
  $types['default']->type = 'default';

  foreach ($types as $type) {
    if (variable_get('gallery_assist_'. $type->type, 0) == 1) {
      $data = variable_get('gallery_assist_'. $type->type .'_data', FALSE);
      if (gallery_assist_generate_ic_presets($type->type, $data)) {
        drupal_set_message('Settings for the gallery assigment "'. $type->type .'" has been saved.');
      }
    }
  }
  variable_set('gallery_assist_imagecache_uninstalled', 0);

  return TRUE;
}

/**
 * Implementation of hook_uninstall().
 */
function gallery_assist_ic_uninstall() {
  $types = node_get_types('types');

  foreach ($types as $type) {
    $ic_data = variable_get('gallery_assist_'. $type->type .'_ic_data', FALSE);
    $data = variable_get('gallery_assist_'. $type->type .'_data', FALSE);
    $data['force_image_height'] = 0;
    $data['upload_thm'] = $ic_data['thm_holder'];
    $data['upload_prev'] = $ic_data['prev_holder'];
    $data['thumbnail_size'] = $ic_data['thm_holder'];
    $data['preview_size'] = $ic_data['prev_holder'];
    variable_set('gallery_assist_'. $type->type .'_data', $data);
    variable_del('gallery_assist_'. $type->type .'_ic_data');
  }
  
  @db_query("TRUNCATE TABLE {cache_gallery_assist_data}");

  variable_del('gallery_assist_use_imagecache');
  variable_set('gallery_assist_imagecache_uninstalled', 1);

  return TRUE;
}

/**
 * Generate and manage imagecache presets for existing galleries from gallery assist or assigments. 
 */
function gallery_assist_generate_ic_presets(&$type, &$data = array()) {
  module_load_include('inc', 'gallery_assist_ic', 'gallery_assist_ic_presets');

  return gallery_assist_presets_maker($type, $data);
}
